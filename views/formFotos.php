
<?php
include_once "dao/fotoDao.php";

$id         = 0;
$nome       = "";
$descricao  = "";

if(isset($_FILES['foto'])){
    $nome = $_POST['nome'];
    $foto = $_FILES['foto'];

    $fields = [
        "nome","diretorio"
    ];
    $values = [
        "'$nome'",$foto
    ];

    if($_POST['id'] == 0){
        if((new FotoDao())->insert($fields,$values)){
            header("Location:visualizarFotos.php");
        }else{
            echo "Deu erro";
        }
    }else{
        if((new FotoDao())->update($fields,$values,$_POST['id'])){
            header("Location:visualizarFotos.php");
        }else{
            echo "Deu erro";
        }
    }

}

if(isset($_GET['id'])){
    $fotos = (new FotoDao())->find($_GET['id']);

    $nome = $fotos['nome'];
    $id = $fotos['id'];
}

?>

<br>
<br>
<br>

<div class="container">
    <form action="" method="POST" role="form" enctype="multipart/form-data">
        <legend>Página de upload</legend>

        <input type="hidden" name="id" value="<?=$id?>">
        <div class="form-group">
            <label for="">Nome</label>
            <input type="text" class="form-control" name="nome" value="<?=$nome?>" id="" placeholder="Digite o nome do documento">
        </div>
        <div class="form-group">
            <label for="">Documento</label>
            <input type="file" name="foto" class="form-control" id="">
        </div>

        <button type="submit" class="btn btn-primary">Enviar</button>
    </form>
</div>


