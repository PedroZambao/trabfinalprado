<section class="pt-5">
<div class="wow fadeIn">
  <h2 class="h1 text-center mb-5">What is MDB?</h2>
  <p class="text-center">MDB is world's most popular Material Design framework for building responsive,
    mobile-first websites
    and apps. </p>
  <p class="text-center mb-5 pb-5">Trusted by over
    <strong>400 000</strong> developers and designers. Easy to use & customize. 400+ material UI elements,
    templates
    & tutorials.</p>
</div>
<div class="row wow fadeIn">
  <div class="col-lg-5 col-xl-4 mb-4">
    <div class="view overlay rounded z-depth-1-half">
      <div class="view overlay">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/cXTThxoywNQ" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
    <h3 class="mb-3 font-weight-bold dark-grey-text">
      <strong>MDB Quick Start</strong>
    </h3>
    <p class="grey-text">Get started with MDBootstrap, the world's most popular Material Design framework for
      building responsive,
      mobile-first sites.</p>
    <p>
      <strong>5 minutes, a few clicks and... done. You will be surprised at how easy it is.</strong>
    </p>
    <a href="https://www.youtube.com/watch?v=cXTThxoywNQ" target="_blank" class="btn btn-primary btn-md">Start
      tutorial
      <i class="fas fa-play ml-2"></i>
    </a>
  </div>
</div>
</section>
</div>
</main>
