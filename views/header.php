<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Gerenciador de Arquivos</title>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
			<link href="assets/css/bootstrap.min.css" rel="stylesheet">
			<link href="assets/css/mdb.min.css" rel="stylesheet">
			<link href="assets/css/style.css" rel="stylesheet">
			<style type="text/css">
				html,
				body,
				header,
				.view {
				height: 100%;
				}
				@media (max-width: 740px) {
				html,
				body,
				header,
				.view {
				height: 1050px;
				}
				}
				@media (min-width: 800px) and (max-width: 850px) {
				html,
				body,
				header,
				.view {
					height: 700px;
				}
				}
				@media (min-width: 800px) and (max-width: 850px) {
				.navbar:not(.top-nav-collapse) {
					background: #1C2331 !important;
				}
				}
			</style>				
	</head>
<body>