<nav class="navbar fixed-top navbar-expand-lg primary-color scrolling-navbar">
      <div class="container">
        <a class="navbar-brand white-text" href="index.php" >
          <strong>Gerenciador de Arquivos</strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
              <a class="nav-link white-text" href="formFotos.php">Upload
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link white-text" href="visualizarFotos.php" >Documentos</a>
            </li>            
          </ul>
						<form class="form-inline white-text" method="post" action="visualizarFotos.php">
      				<div class="md-form my-0 white-text">
        				<input class="form-control mr-sm-2 " name="searchFoto" type="text" placeholder="Pesquisar" aria-label="Pesquisar">
      				</div>
    				</form>
          </ul>
        </div>
      </div>
  </nav>
