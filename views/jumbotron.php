    <div class="view full-page-intro">
      <video class="video-intro" autoplay loop muted>
        <source src="https://mdbootstrap.com/img/video/animation-intro.mp4" type="video/mp4" />
      </video>
      <div class="mask rgba-blue-light d-flex justify-content-center align-items-center">
        <div class="container">
          <div class="row d-flex h-100 justify-content-center align-items-center wow fadeIn">
            <div class="col-md-8 mb-4 white-text text-center text-md-left">
              <h1 class="display-4 font-weight-bold">Gerenciador de Arquivos</h1>
              <p>
                <strong>Visualize seus documentos em qualquer hora</strong>
              </p>
              <p class="mb-4 d-none d-md-block">
                <strong>Cada vez mais é necessario termos acesso aos nossos documentos de modo transparente, ou seja, independente da hora, dispositivo ou lugar.</strong>
              </p>
              <a href="formFotos.php" class="btn btn-outline-white">Enviar documentos
                <i class="fas fa-cloud-upload-alt ml-2"></i>
              </a>
              <a href="visualizarFotos.php" class="btn btn-outline-white">Visualizar documentos
                <i class="fas fa-eye ml-2"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>


	
	

      