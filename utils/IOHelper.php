<?php

class IOHelper{
    public static function uploadFile($file){

        $filename = $file["name"];
        $filetype = $file["type"];
        $filesize = $file["size"];

        try{
            move_uploaded_file($file["tmp_name"], UPLOAD_PATH."".$filename);

            return UPLOAD_PATH."".$filename;
        }catch(Exception $e){
            return null;
        }
    }
}

?>