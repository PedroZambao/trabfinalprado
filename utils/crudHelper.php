<?php
    class CrudHelper{
        public static function buildInsertQuery($fields,$values){

            $fieldValues = [];

            $fieldValues["fields"] = implode(",",$fields);
            $fieldValues["values"] = implode(",",$values);

            return $fieldValues;
        }

        public static function buildUpdateQuery($fields,$values){
            $fieldsUpdate = "";
        
            foreach($fields as $index => $field){
                $fieldsUpdate .= "$field = $values[$index],";
            }

            return rtrim($fieldsUpdate,",");
        }
    }
?>