<?php

include_once "config/database.php";
include_once "interfaces/CRUD.php";
include_once "utils/crudHelper.php";
include_once "utils/IOHelper.php";

class FotoDao extends Database implements CRUD{

    private $tableName = "foto";

    function __construct(){
        parent::connect();
    }

    public function select(){
        $sql = "SELECT * FROM $this->tableName";

        return parent::fetchAll($sql);
    }

    public function find($id){
        $sql = "SELECT * FROM $this->tableName WHERE id = $id";

        //die($sql);
        $fotos = parent::fetchAll($sql);
        if(!empty($fotos)){
            return $fotos[0];
        }

        return null;
    }

    public function insert($fields = [], $values = []){

        foreach($values as $index => &$value){
            if(is_array($value)){
                if($value['size'] > 0){
                    $value = IOHelper::uploadFile($value);
                    $value = $value == null ? null : "'".$value."'";
                }else{
                    unset($values[$index]);
                    unset($fields[$index]);
                }
            }
        }

        $fieldsValues = CrudHelper::buildInsertQuery($fields,$values);

        $fields = $fieldsValues["fields"];
        $values = $fieldsValues["values"];

        $sql = "INSERT INTO $this->tableName ($fields) values($values)";

        return parent::executeQuery($sql);
    }
    public function update($fields = [], $values = [],$id){

        foreach($values as $index => &$value){
            if(is_array($value)){
                if($value['size'] > 0){
                    $value = IOHelper::uploadFile($value);
                    $value = $value == null ? null : "'".$value."'";
                }else{
                    unset($values[$index]);
                    unset($fields[$index]);
                }
            }
        }

        $fieldsUpdate = CrudHelper::buildUpdateQuery($fields,$values);

        $sql = "UPDATE $this->tableName SET $fieldsUpdate WHERE id = $id";

        //die($sql);
        return parent::executeQuery($sql);

    }
    public function delete($id){
        
        $sql = "DELETE FROM $this->tableName WHERE id = $id";


        return parent::executeQuery($sql);

    }

    public function searchFoto($nome){

        $sql = "SELECT * FROM $this->tableName WHERE nome LIKE '%$nome%'";

        //die($sql);
        
        return parent::fetchAll($sql);
    }

}
?>