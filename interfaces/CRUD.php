<?php
interface CRUD{
    public function select();
    public function insert($fields = [], $values = []);
    public function update($fields = [], $values = [],$id);
    public function delete($id);
    public function find($id);
}
?>