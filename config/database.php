<?php

abstract class Database{

    private $server = "";
    private $username = "";
    private $db = "";
    private $password = "";

    protected $pdo;

    protected function connect(){

        $this->server = "localhost";
        $this->username = "root";
        $this->db = "gerenciadorarquivo";
        $this->password = "";

        try {
            $this->pdo = new PDO("mysql:host=$this->server;dbname=$this->db", $this->username, $this->password);
		    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}catch(PDOException $e){
            die($e->getMessage());
			$this->pdo = null;
	    }
    }

    protected function executeQuery($sql){

        $stmt = $this->pdo->prepare($sql);

        return $stmt->execute();
    }

    protected function fetchAll($sql){

        $stmt = $this->pdo->prepare($sql);

        $stmt->execute();

        return $stmt->fetchAll();
    }
    
}

?>